// bootstrap
@256
D=A
@SP
M=D


@Sys.init.return.0
D=A
@SP
A=M
M=D
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
@SP
M=M+1
@R3
D=M
@SP
A=M
M=D
@SP
M=M+1
@R4
D=M
@SP
A=M
M=D
@SP
M=M+1
@SP
D=M
@0
D=D-A
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.init
0;JMP
(Sys.init.return.0)


// function Sys.init 0
(Sys.init)
// push constant 4000
@4000
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 0
@R3
D=A
@0
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 5000
@5000
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 1
@R3
D=A
@1
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// call Sys.main 0
@Sys.main.return.1
D=A
@SP
A=M
M=D
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
@SP
M=M+1
@R3
D=M
@SP
A=M
M=D
@SP
M=M+1
@R4
D=M
@SP
A=M
M=D
@SP
M=M+1
@SP
D=M
@0
D=D-A
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.main
0;JMP
(Sys.main.return.1)
// pop temp 1
@R5
D=A
@1
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// label LOOP
(Sys.init$LOOP)
// goto LOOP
@Sys.init$LOOP
D;JMP
// function Sys.main 5
(Sys.main)
@0
D=A
@SP
A=M
M=D
@SP
M=M+1
@0
D=A
@SP
A=M
M=D
@SP
M=M+1
@0
D=A
@SP
A=M
M=D
@SP
M=M+1
@0
D=A
@SP
A=M
M=D
@SP
M=M+1
@0
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 4001
@4001
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 0
@R3
D=A
@0
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 5001
@5001
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 1
@R3
D=A
@1
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 200
@200
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop local 1
@LCL
A=M
D=A
@1
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 40
@40
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop local 2
@LCL
A=M
D=A
@2
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 6
@6
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop local 3
@LCL
A=M
D=A
@3
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 123
@123
D=A
@SP
A=M
M=D
@SP
M=M+1
// call Sys.add12 1
@Sys.add12.return.2
D=A
@SP
A=M
M=D
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
@SP
M=M+1
@R3
D=M
@SP
A=M
M=D
@SP
M=M+1
@R4
D=M
@SP
A=M
M=D
@SP
M=M+1
@SP
D=M
@1
D=D-A
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.add12
0;JMP
(Sys.add12.return.2)
// pop temp 0
@R5
D=A
@0
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push local 0
@LCL
D=M
@0
D=D+A
A=D
D=M
@SP
A=M
M=D
@SP
M=M+1
// push local 1
@LCL
D=M
@1
D=D+A
A=D
D=M
@SP
A=M
M=D
@SP
M=M+1
// push local 2
@LCL
D=M
@2
D=D+A
A=D
D=M
@SP
A=M
M=D
@SP
M=M+1
// push local 3
@LCL
D=M
@3
D=D+A
A=D
D=M
@SP
A=M
M=D
@SP
M=M+1
// push local 4
@LCL
D=M
@4
D=D+A
A=D
D=M
@SP
A=M
M=D
@SP
M=M+1
// add
@SP
A=M-1
A=A-1
D=M
A=A+1
D=D+M
@SP
A=M-1
A=A-1
M=D
@SP
M=M-1
// add
@SP
A=M-1
A=A-1
D=M
A=A+1
D=D+M
@SP
A=M-1
A=A-1
M=D
@SP
M=M-1
// add
@SP
A=M-1
A=A-1
D=M
A=A+1
D=D+M
@SP
A=M-1
A=A-1
M=D
@SP
M=M-1
// add
@SP
A=M-1
A=A-1
D=M
A=A+1
D=D+M
@SP
A=M-1
A=A-1
M=D
@SP
M=M-1
// return
@LCL
D=M
@5
D=D-A
A=D
D=M
@v_temp_return
M=D
@ARG
A=M
D=A
@0
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
@ARG
D=M+1
@SP
M=D
@LCL
D=M
@1
D=D-A
A=D
D=M
@R4
M=D
@LCL
D=M
@2
D=D-A
A=D
D=M
@R3
M=D
@LCL
D=M
@3
D=D-A
A=D
D=M
@ARG
M=D
@LCL
D=M
@4
D=D-A
A=D
D=M
@LCL
M=D
@v_temp_return
D=M
A=D
0;JMP
// function Sys.add12 0
(Sys.add12)
// push constant 4002
@4002
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 0
@R3
D=A
@0
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 5002
@5002
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 1
@R3
D=A
@1
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push argument 0
@ARG
D=M
@0
D=D+A
A=D
D=M
@SP
A=M
M=D
@SP
M=M+1
// push constant 12
@12
D=A
@SP
A=M
M=D
@SP
M=M+1
// add
@SP
A=M-1
A=A-1
D=M
A=A+1
D=D+M
@SP
A=M-1
A=A-1
M=D
@SP
M=M-1
// return
@LCL
D=M
@5
D=D-A
A=D
D=M
@v_temp_return
M=D
@ARG
A=M
D=A
@0
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
@ARG
D=M+1
@SP
M=D
@LCL
D=M
@1
D=D-A
A=D
D=M
@R4
M=D
@LCL
D=M
@2
D=D-A
A=D
D=M
@R3
M=D
@LCL
D=M
@3
D=D-A
A=D
D=M
@ARG
M=D
@LCL
D=M
@4
D=D-A
A=D
D=M
@LCL
M=D
@v_temp_return
D=M
A=D
0;JMP
// function Sys.init 0
(Sys.init)
// push constant 4000
@4000
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 0
@R3
D=A
@0
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 5000
@5000
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 1
@R3
D=A
@1
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// call Sys.main 0
@Sys.main.return.3
D=A
@SP
A=M
M=D
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
@SP
M=M+1
@R3
D=M
@SP
A=M
M=D
@SP
M=M+1
@R4
D=M
@SP
A=M
M=D
@SP
M=M+1
@SP
D=M
@0
D=D-A
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.main
0;JMP
(Sys.main.return.3)
// pop temp 1
@R5
D=A
@1
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// label LOOP
(Sys.init$LOOP)
// goto LOOP
@Sys.init$LOOP
D;JMP
// function Sys.main 5
(Sys.main)
@0
D=A
@SP
A=M
M=D
@SP
M=M+1
@0
D=A
@SP
A=M
M=D
@SP
M=M+1
@0
D=A
@SP
A=M
M=D
@SP
M=M+1
@0
D=A
@SP
A=M
M=D
@SP
M=M+1
@0
D=A
@SP
A=M
M=D
@SP
M=M+1
// push constant 4001
@4001
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 0
@R3
D=A
@0
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 5001
@5001
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 1
@R3
D=A
@1
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 200
@200
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop local 1
@LCL
A=M
D=A
@1
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 40
@40
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop local 2
@LCL
A=M
D=A
@2
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 6
@6
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop local 3
@LCL
A=M
D=A
@3
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 123
@123
D=A
@SP
A=M
M=D
@SP
M=M+1
// call Sys.add12 1
@Sys.add12.return.4
D=A
@SP
A=M
M=D
@SP
M=M+1
@LCL
D=M
@SP
A=M
M=D
@SP
M=M+1
@ARG
D=M
@SP
A=M
M=D
@SP
M=M+1
@R3
D=M
@SP
A=M
M=D
@SP
M=M+1
@R4
D=M
@SP
A=M
M=D
@SP
M=M+1
@SP
D=M
@1
D=D-A
@5
D=D-A
@ARG
M=D
@SP
D=M
@LCL
M=D
@Sys.add12
0;JMP
(Sys.add12.return.4)
// pop temp 0
@R5
D=A
@0
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push local 0
@LCL
D=M
@0
D=D+A
A=D
D=M
@SP
A=M
M=D
@SP
M=M+1
// push local 1
@LCL
D=M
@1
D=D+A
A=D
D=M
@SP
A=M
M=D
@SP
M=M+1
// push local 2
@LCL
D=M
@2
D=D+A
A=D
D=M
@SP
A=M
M=D
@SP
M=M+1
// push local 3
@LCL
D=M
@3
D=D+A
A=D
D=M
@SP
A=M
M=D
@SP
M=M+1
// push local 4
@LCL
D=M
@4
D=D+A
A=D
D=M
@SP
A=M
M=D
@SP
M=M+1
// add
@SP
A=M-1
A=A-1
D=M
A=A+1
D=D+M
@SP
A=M-1
A=A-1
M=D
@SP
M=M-1
// add
@SP
A=M-1
A=A-1
D=M
A=A+1
D=D+M
@SP
A=M-1
A=A-1
M=D
@SP
M=M-1
// add
@SP
A=M-1
A=A-1
D=M
A=A+1
D=D+M
@SP
A=M-1
A=A-1
M=D
@SP
M=M-1
// add
@SP
A=M-1
A=A-1
D=M
A=A+1
D=D+M
@SP
A=M-1
A=A-1
M=D
@SP
M=M-1
// return
@LCL
D=M
@5
D=D-A
A=D
D=M
@v_temp_return
M=D
@ARG
A=M
D=A
@0
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
@ARG
D=M+1
@SP
M=D
@LCL
D=M
@1
D=D-A
A=D
D=M
@R4
M=D
@LCL
D=M
@2
D=D-A
A=D
D=M
@R3
M=D
@LCL
D=M
@3
D=D-A
A=D
D=M
@ARG
M=D
@LCL
D=M
@4
D=D-A
A=D
D=M
@LCL
M=D
@v_temp_return
D=M
A=D
0;JMP
// function Sys.add12 0
(Sys.add12)
// push constant 4002
@4002
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 0
@R3
D=A
@0
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push constant 5002
@5002
D=A
@SP
A=M
M=D
@SP
M=M+1
// pop pointer 1
@R3
D=A
@1
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
// push argument 0
@ARG
D=M
@0
D=D+A
A=D
D=M
@SP
A=M
M=D
@SP
M=M+1
// push constant 12
@12
D=A
@SP
A=M
M=D
@SP
M=M+1
// add
@SP
A=M-1
A=A-1
D=M
A=A+1
D=D+M
@SP
A=M-1
A=A-1
M=D
@SP
M=M-1
// return
@LCL
D=M
@5
D=D-A
A=D
D=M
@v_temp_return
M=D
@ARG
A=M
D=A
@0
D=D+A
@v_temp_pop_dest
M=D
@SP
A=M-1
D=M
@v_temp_pop_dest
A=M
M=D
@SP
M=M-1
@ARG
D=M+1
@SP
M=D
@LCL
D=M
@1
D=D-A
A=D
D=M
@R4
M=D
@LCL
D=M
@2
D=D-A
A=D
D=M
@R3
M=D
@LCL
D=M
@3
D=D-A
A=D
D=M
@ARG
M=D
@LCL
D=M
@4
D=D-A
A=D
D=M
@LCL
M=D
@v_temp_return
D=M
A=D
0;JMP
