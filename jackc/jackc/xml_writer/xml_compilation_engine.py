from jackc.tokenizer import JackTokenizer, Token
import os


def _token_is_field(token):
    return token.src == "static" or token.src == "field"


def _token_is_subroutine(token: Token):
    return token.src == "constructor" or token.src == "function" or token.src == "method"


def _token_is_statement(token: Token):
    return token.src == "let" or token.src == "if" or token.src == "while" or token.src == "do" \
           or token.src == "return"


ops = ['+', '-', '*', '/', '&', '|', '<', '>', '=']
unary_op = ['-', '~']
keyword_constant = ['true', 'false', 'null', 'this']


class XmlCompilationEngine:
    def __init__(self, src_path, output_path):
        self.tokenizer = JackTokenizer(src_path)

        out_folder = os.path.dirname(output_path)
        if not os.path.exists(out_folder):
            os.makedirs(out_folder)
        self.out_file = open(output_path, "w")
        self.ahead_token = None

    def compile_class(self):
        self._write("<class>")
        self._write_next_token()
        self._write_next_token()
        self._write_next_token()
        self.compile_class_var_dec()
        self.compile_subroutine()
        self._write_next_token("}")
        self._write("</class>")

    def compile_class_var_dec(self):
        token = self._next()
        if token and _token_is_field(token):
            self._write("<classVarDec>")
            self._write_token(token)
            self._write_next_token()
            self._write_next_token()
            token = self._next()
            while token.src == ",":
                self._write_token(token)
                self._write_next_token()
                token = self._next()
            self._write_token(token)
            self._write("</classVarDec>")
            self.compile_class_var_dec()
        else:
            self.putback(token)

    def compile_subroutine(self):
        token = self._read_ahead_next()
        while token and _token_is_subroutine(token):
            self._write("<subroutineDec>")
            # function
            self._write_next_token()
            # type
            self._write_next_token()
            # function_name
            self._write_next_token()
            self._write_next_token('(')
            self.compile_parameter_list()
            self._write_next_token(')')
            # subroutineBody
            self._write_tag_begin("subroutineBody")
            self._write_next_token('{')
            while self._read_ahead_next().src == "var":
                self.compile_var_dec()
            self.compile_statements()
            self._write_next_token('}')
            self._write_tag_end("subroutineBody")
            self._write("</subroutineDec>")
            token = self._read_ahead_next()

    def compile_parameter_list(self):
        self._write("<parameterList>")
        token = self._read_ahead_next()
        if token.src != ")":
            self._write_next_token()
            self._write_next_token()
            token = self._read_ahead_next()
            while token.src == ",":
                self._write_next_token()
                self._write_next_token()
                self._write_next_token()
                token = self._read_ahead_next()
        self._write("</parameterList>")

    def compile_var_dec(self):
        self._write_tag_begin("varDec")
        self._write_next_token("var")
        self._write_next_token()
        self._write_next_token()
        while self._read_ahead_next().src == ",":
            self._write_next_token()
            self._write_next_token()
        self._write_next_token()
        self._write_tag_end("varDec")

    def compile_statements(self):
        self._write_tag_begin("statements")
        token = self._read_ahead_next()
        while token and _token_is_statement(token):
            if token.src == "let":
                self.compile_let()
            elif token.src == "if":
                self.compile_if()
            elif token.src == "while":
                self.compile_while()
            elif token.src == "do":
                self.compile_do()
            elif token.src == "return":
                self.compile_return()
            token = self._read_ahead_next()
        self._write_tag_end("statements")

    def compile_do(self):
        self._write_tag_begin("doStatement")
        self._write_next_token("do")
        # subroutineCall
        self._compile_subroutine_call()
        self._write_next_token(";")
        self._write_tag_end("doStatement")

    def compile_let(self):
        self._write_tag_begin("letStatement")
        self._write_next_token()
        self._write_next_token()
        token = self._read_ahead_next()
        if token.src == "[":
            self._write_next_token("[")
            self.compile_expression()
            self._write_next_token("]")
        self._write_next_token("=")
        self.compile_expression()
        self._write_next_token(";")
        self._write_tag_end("letStatement")

    def compile_while(self):
        self._write_tag_begin("whileStatement")
        self._compile_if_or_while()
        self._write_tag_end("whileStatement")

    def _compile_if_or_while(self):
        """if and while have same constructor"""
        self._write_next_token()
        self._write_next_token()
        self.compile_expression()
        self._write_next_token()
        self._write_next_token()
        self.compile_statements()
        self._write_next_token()

    def compile_return(self):
        self._write_tag_begin("returnStatement")
        self._write_next_token()
        token = self._next()
        self.putback(token)
        if token.src != ";":
            self.compile_expression()
        self._write_next_token()
        self._write_tag_end("returnStatement")

    def compile_if(self):
        self._write_tag_begin("ifStatement")
        self._compile_if_or_while()
        if self._read_ahead_next().src == "else":
            self._write_next_token("else")
            self._write_next_token("{")
            self.compile_statements()
            self._write_next_token("}")
        self._write_tag_end("ifStatement")

    def compile_expression(self):
        self._write_tag_begin("expression")
        self.compile_term()
        token = self._read_ahead_next()
        while token and token.src in ops:
            self._write_next_token()
            self.compile_term()
            token = self._read_ahead_next()
        self._write_tag_end("expression")

    def compile_term(self):
        self._write_tag_begin("term")
        token = self._read_ahead_next()
        t_type = token.token_type()
        # integerConstant, stringConstant, keywordConstant
        if t_type == Token.T_INT_CONST or t_type == Token.T_STRING_CONST or token.src in keyword_constant:
            self._write_next_token()
        # unaryOp term
        elif token.src in unary_op:
            self._write_next_token()
            self.compile_term()
        # (expression)
        elif token.src == "(":
            self._write_next_token()
            self.compile_expression()
            self._write_next_token()
        else:
            token = self._next()
            next_token = self._read_ahead_next()
            # varName[expression]
            if next_token.src == "[":
                self._write_token(token)
                self._write_next_token()
                self.compile_expression()
                self._write_next_token()
            # subroutineCall
            elif next_token.src == "(" or next_token.src == '.':
                self._compile_subroutine_call(token)
            else:
                self. _write_token(token)
        self._write_tag_end("term")

    def _compile_subroutine_call(self, pre_token: Token = None):
        token = pre_token or self._next()
        self._write_token(token)
        token = self._read_ahead_next()
        if token.src == "(":
            self._write_next_token("(")
            self.compile_expression_list()
            self._write_next_token(")")
        else:
            self._write_next_token(".")
            self._write_next_token()
            self._write_next_token("(")
            self.compile_expression_list()
            self._write_next_token(")")

    def compile_expression_list(self):
        self._write_tag_begin("expressionList")
        token = self._read_ahead_next()
        if token.src != ")":
            self.compile_expression()
            token = self._read_ahead_next()
            while token.src == ",":
                self._write_next_token()
                self.compile_expression()
                token = self._read_ahead_next()
        self._write_tag_end("expressionList")

    def _write_next_token(self, expect: str = ""):
        self._write_token(self._next(), expect)

    def _write_token(self, token, expect: str = ""):
        if expect:
            if token.src != expect:
                raise Exception(f"want {expect}, but got {token.src}")

        self._write(token.xml())

    def _read_ahead_next(self):
        rst = self._next()
        self.putback(rst)
        return rst

    def _next(self) -> Token or None:
        if self.ahead_token:
            rst = self.ahead_token
            self.ahead_token = None
            return rst
        else:
            if self.tokenizer.has_more_tokens():
                return self.tokenizer.advance()
            else:
                return None

    def putback(self, token):
        if self.ahead_token:
            raise Exception("Why put back twice?")
        else:
            self.ahead_token = token

    def _write(self, string):
        print(string, file=self.out_file)

    def _write_tag_begin(self, tag):
        self._write(f"<{tag}>")

    def _write_tag_end(self, tag):
        self._write(f"</{tag}>")
