import os


class Parser:
    def __init__(self, assemble_file_path):
        self.assemble_file_path = assemble_file_path
        if not os.path.exists(self.assemble_file_path):
            raise RuntimeError("the file {} not exist.".format(self.assemble_file_path))
        self.assemble_file = open(self.assemble_file_path)
        self.cur_line = ""
        self.cur_order = None

    def _has_more_commands(self):
        """输入中还有更多命令吗"""
        self.cur_line = self.assemble_file.readline()
        if not self.cur_line:
            return False

        self.cur_line = self.cur_line.replace(" ", "").replace("\n", "").replace("\t", "")
        comment_index = self.cur_line.find("//")
        if comment_index >= 0:
            self.cur_line = self.cur_line[0: comment_index]
        if self.cur_line:
            return True
        else:
            return self._has_more_commands()

    def advance(self):
        if self._has_more_commands():
            return Command(self.cur_line)
        else:
            return None


class Command:
    A_COMMAND = 0
    C_COMMAND = 1
    L_COMMAND = 2
    """A machine order"""
    def __init__(self, src: str):
        self.src = src
        self._parse()

    def _parse(self):
        if self.src.startswith("@"):
            self.type = Command.A_COMMAND
        elif self.src.startswith("("):
            self.type = Command.L_COMMAND
        else:
            self.type = Command.C_COMMAND

    def command_type(self):
        return self.type

    def symbol(self):
        if self.type == Command.A_COMMAND:
            return self.src[1:]
        elif self.type == Command.L_COMMAND:
            return self.src[1:-1]
        else:
            raise Exception("C_COMMAND has no symbol")

    def dest(self):
        if self.type == Command.C_COMMAND:
            if "=" not in self.src:
                return ""
            else:
                return self.src.split("=")[0]
        else:
            raise Exception("Only C_COMMAND has dest")

    def comp(self):
        if self.type == Command.C_COMMAND:
            if "=" not in self.src:
                remove_dest = self.src
            else:
                remove_dest = self.src.split("=")[1]

            return remove_dest.split(";")[0]
        else:
            raise Exception("Only C_COMMAND has comp")

    def jump(self):
        if self.type == Command.C_COMMAND:
            if ";" not in self.src:
                return ""
            else:
                return self.src.split(";")[1]
        else:
            raise Exception("Only C_COMMAND has jump")

    def __str__(self) -> str:
        return self.src
