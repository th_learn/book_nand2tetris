
// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

// The algorithm
// while(ture) {
//   if (pressed) {
//     screen[pos++] = black
//   } else {
//     screen[--pos] = white 
//   }
// }
@SCREEN
D=A
@i
M=D
@j
M=0

@SCREEN
D=A
@clean_index
M=D
@black_index
M=0
@black_value
M=1
@black_value
D=A
@black_index
M=D

(INIT_BLACK_LOOP)
@black_index
D=M
@black_value
D=D-A
@15
D=D-A
@INIT_BLACK_END
D;JGE

@black_index
A=M
D=M
A=D
D=D+A
D=D+1
@black_index
A=M
A=A+1
M=D
@black_index
M=M+1
@INIT_BLACK_LOOP
0;JMP

(INIT_BLACK_END)


(LOOP)
@KBD
D=M
@CLEAN
D;JEQ

// set black begin
// if i == 24575 and j == 16, means full
@i
D=M
@24575
D=D-A
@SET_BLACK
D;JNE

@j
D=M
@16
D=D-A
@SET_BLACK
D;JNE
@LOOP
0;JMP

(SET_BLACK)
@j
D=M
@16
D=D-A
@AFTER_HANLE_J
D;JLT

@i
M=M+1
@j
M=0

(AFTER_HANLE_J)
@j
D=M
@black_value
A=D+A
D=M
@i
A=M
M=D

@j
M=M+1

@LOOP
0;JMP


// // clean screen begin
(CLEAN)


(CLEAN_END)
@LOOP
0;JMP
