import os


class Parser:
    def __init__(self, vm_src_path):
        self.vm_src_path = vm_src_path
        if not os.path.exists(self.vm_src_path):
            raise Exception(f"Can't open file: {self.vm_src_path}")
        self.vm_src = open(self.vm_src_path)
        self.cur_src = ""
        self.cur_command = None

    def has_more_commands(self):
        self.cur_src = self.vm_src.readline()
        if not self.cur_src:
            return False
        self.cur_src = self.cur_src.strip(" ").replace("\n", "").replace("\t", "")
        comment_index = self.cur_src.find("//")
        if comment_index >= 0:
            self.cur_src = self.cur_src[0: comment_index]
        if self.cur_src:
            return True
        else:
            return self.has_more_commands()

    def advance(self):
        self.cur_command = Command(self.cur_src)
        return self.cur_command


ARITHMETIC_OPS = ["add", "sub", "neg", "eq", "lt", "gt", "and", "or", "not"]


class Command:
    C_ARITHMETIC = 0,
    C_PUSH = 1,
    C_POP = 2,
    C_LABEL = 3
    C_GOTO = 4
    C_IF = 5
    C_FUNCTION = 6
    C_RETURN = 7,
    C_CALL = 8
    OP_TYPE_MAP = {
        "push": C_PUSH,
        "pop": C_POP,
        "label": C_LABEL,
        "if-goto": C_IF,
        "goto": C_GOTO,
        "function": C_FUNCTION,
        "call": C_CALL,
        "return": C_RETURN
    }

    def __init__(self, src: str):
        self.src = src
        self._parse()

    def _parse(self):
        self.ops = list(filter(lambda x: x, self.src.split(" ")))
        self.op = self.ops[0]

        if self.op in ARITHMETIC_OPS:
            self._type = Command.C_ARITHMETIC
        elif self.op in Command.OP_TYPE_MAP:
            self._type = Command.OP_TYPE_MAP[self.op]
        else:
            raise Exception(f"unknown command type: {self.op}")

    def c_type(self):
        return self._type

    def arg1(self):
        if self.c_type() == Command.C_ARITHMETIC:
            return self.ops[0]
        else:
            return self.ops[1]

    def arg2(self):
        if self._type == Command.C_PUSH or self._type == Command.C_POP or self._type == Command.C_FUNCTION \
                or self._type == Command.C_CALL:
            return self.ops[2]
        else:
            raise Exception(f"{self.src} has no arg2")
