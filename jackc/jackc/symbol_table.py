from __future__ import annotations
from enum import auto, IntEnum, unique
from typing import Dict, Optional
from jackc.vmwriter import Segment


@unique
class SymbolKind(IntEnum):
    STATIC = 0,
    FIELD = 1,
    ARG = 2,
    VAR = 3


class Symbol:
    def __init__(self, name: str, symbol_type: str, kind: SymbolKind, index):
        self.name = name
        self.symbol_type = symbol_type
        self.kind = kind
        self.index = index


class SymbolTable:
    def __init__(self):
        self._class_global_table: Dict[str, Symbol] = {}
        self._func_table: Dict[str, Symbol] = {}
        self._index_map: Dict[SymbolKind, int] = {
            SymbolKind.FIELD: 0,
            SymbolKind.STATIC: 0,
            SymbolKind.ARG: 0,
            SymbolKind.VAR: 0,
        }

    def start_subroutine(self):
        self._func_table.clear()
        self._index_map[SymbolKind.ARG] = 0
        self._index_map[SymbolKind.VAR] = 0

    def define(self, name: str, symbol_type: str, kind: SymbolKind):
        table = self._get_table(kind)
        if name in table:
            raise Exception(f"why {name} put twice?")
        table[name] = Symbol(name, symbol_type, kind, self._get_define_index(kind))

    def var_count(self, kind: SymbolKind) -> int:
        return self._index_map[kind]

    def kink_of(self, name: str) -> Optional[SymbolKind]:
        symbol = self._find_symbol(name)
        if symbol:
            return symbol.kind
        return None

    def type_of(self, name: str) -> str:
        symbol = self._find_symbol(name)
        if symbol:
            return symbol.symbol_type
        return ""

    def index_of(self, name) -> int:
        symbol = self._find_symbol(name)
        if symbol:
            return symbol.index
        return -1

    def _get_table(self, kind: SymbolKind) -> Dict[str, Symbol]:
        if kind == SymbolKind.ARG or kind == SymbolKind.VAR:
            return self._func_table
        else:
            return self._class_global_table

    def _get_define_index(self, kind: SymbolKind):
        rst = self._index_map[kind]
        self._index_map[kind] = rst + 1
        return rst

    def _find_symbol(self, name) -> Optional[Symbol]:
        if name in self._func_table:
            return self._func_table[name]
        elif name in self._class_global_table:
            return self._class_global_table[name]
        else:
            return None

    def find_symbol(self, name: str) -> Symbol:
        rst = self._find_symbol(name)
        if not rst:
            raise Exception(f"can't find symbol {name}")
        return rst

