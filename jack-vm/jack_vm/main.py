import sys
import os
from jack_vm.parser import Parser, Command
from jack_vm.code_writer import CodeWrite


def _write_file_to_code_writer(src_file_path: str, code_writer: CodeWrite):
    parser = Parser(src_file_path)
    while parser.has_more_commands():
        command = parser.advance()
        code_writer.set_cur_src_path(src_file_path)
        code_writer.write_comment(command.src)
        code_writer.write(command)


def do_single_file(src_path):
    if not src_path.endswith(".vm"):
        print(f"{src_path} not ends with .vm")
        return
    dst_path = os.path.splitext(src_path)[0] + ".asm"
    code_writer = CodeWrite()
    code_writer.set_asm_file_path(dst_path)
    _write_file_to_code_writer(src_path, code_writer)
    code_writer.close()


def do_folder(folder_path):
    sys_vm_path = os.path.join(folder_path, "Sys.vm")
    asm_file_name = os.path.basename(os.path.dirname(folder_path)) + ".asm"
    asm_file_path = os.path.join(folder_path, asm_file_name)

    code_writer = CodeWrite()
    code_writer.set_asm_file_path(asm_file_path)
    code_writer.write_init()

    _write_file_to_code_writer(sys_vm_path, code_writer)

    other_vms = os.listdir(folder_path)
    for vm_file in other_vms:
        if vm_file.endswith(".vm"):
            _write_file_to_code_writer(os.path.join(folder_path, vm_file), code_writer)

    code_writer.close()


def main():
    if len(sys.argv) < 2:
        print(f"Usage: {sys.argv[0]} src_path [dst]")
        return

    src_path = sys.argv[1]
    if not os.path.exists(src_path):
        print(f"cant' find src file: {src_path}")
        return

    if os.path.isfile(src_path):
        do_single_file(src_path)
    else:
        files = os.listdir(src_path)
        if "Sys.vm" not in files:
            raise Exception(f"folder {src_path} should have Sys.vm")
        do_folder(src_path)
    # if os.path.isdir(src_path):
    #     for root, dirs, files in os.walk(src_path):
    #         for f in files:
    #             if f.endswith(".vm"):
    #                 do_single_file(os.path.join(root, f))
    # else:
    #     do_single_file()


if __name__ == '__main__':
    main()
