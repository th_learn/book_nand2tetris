import os


def _char_is_space_or_new_line(c):
    return c == " " or c == "\n" or c == "\t"


class JackTokenizer:
    K_CLASS = 0,
    K_METHOD = 1,
    K_INT = 2,
    K_FUNCTION = 3,
    K_BOOLEAN = 4,
    K_CONSTRUCTOR = 5,
    K_CHAR = 6,
    K_VOID = 7,
    K_VAR = 8,
    K_STATIC = 9,
    K_FIELD = 10,
    K_LET = 11,
    K_DO = 12,
    K_IF = 13,
    K_ELSE = 14,
    K_WHILE = 15,
    K_RETURN = 16,
    K_TRUE = 17,
    K_FALSE = 18,
    K_NULL = 19,
    K_THIS = 20

    def __init__(self, src_path):
        self.src_path = src_path
        if not os.path.exists(src_path) or not os.path.isfile(src_path):
            raise Exception(f"can't open file {src_path}")
        self.file = open(src_path)
        self.cur_tokens = []
        self.cur_token = None
        self.in_str = False
        self.cur_str = ""
        self.ch_read_ahead = None
        self.cur_line_num = 1

    def has_more_tokens(self):
        self.cur_token = self._read_next_token()
        return self.cur_token is not None

    def advance(self):
        return self.cur_token

    def line_num(self) -> int:
        return self.cur_line_num

    def _read_next_token(self):
        """parse next token."""
        self._skip_all_space_and_new_line()
        ch = self._char()
        if not ch:
            return None

        if ch == '"':
            # print("begin read string")
            string = ""
            ch = self._char()
            while ch != '"':
                if ch == '\n':
                    self._raise_exception("we now not support cross line string")
                string += ch
                ch = self._char()
            return Token(string, Token.T_STRING_CONST)
        elif ch == "/":
            # print("begin read comment")
            next_ch = self._char()
            if next_ch != "/" and next_ch != "*":
                self._putback(next_ch)
                return Token(ch, Token.T_SYMBOL)
            if next_ch == "/":
                ch = self._char()
                while ch != '\n':
                    ch = self._char()
            if next_ch == "*":
                ch1 = self._char()
                ch2 = self._char()
                while not (ch1 == "*" and ch2 == "/"):
                    ch1 = ch2
                    ch2 = self._char()
            return self._read_next_token()
        elif ch in Token.SYMBOLS:
            return Token(ch, Token.T_SYMBOL)
        else:
            string = ch
            if string.isdigit():
                # print("begin read int")
                ch = self._char()
                while ch != " " and ch not in Token.SYMBOLS:
                    if ch == "\n":
                        self._raise_exception("Why digit cross line")
                    elif not ch.isdigit():
                        self._raise_exception("token starts with digit")
                    else:
                        string += ch
                    ch = self._char()
                if ch in Token.SYMBOLS:
                    self._putback(ch)
                return Token(int(string), Token.T_INT_CONST)
            else:
                # now is the keyword or identifier
                ch = self._char()
                # print("begin read identifier")
                while ch != " " and ch not in Token.SYMBOLS:
                    if ch == "\n":
                        self._raise_exception("Why identifier cross line")
                    else:
                        string += ch
                    ch = self._char()
                if ch in Token.SYMBOLS:
                    self._putback(ch)
                return Token(string)

    def _skip_all_space_and_new_line(self):
        ch = self._char()
        while _char_is_space_or_new_line(ch):
            ch = self._char()
        if not _char_is_space_or_new_line(ch):
            self._putback(ch)

    def _putback(self, ch):
        if self.ch_read_ahead:
            raise Exception("raise twice")
        self.ch_read_ahead = ch

    def _char(self):
        if self.ch_read_ahead:
            ch = self.ch_read_ahead
            self.ch_read_ahead = None
        else:
            ch = self.file.read(1)
            if ch == "\n":
                self.cur_line_num += 1
        return ch

    def _raise_exception(self, msg):
        raise Exception(f"{msg} at L{self.cur_line_num}")

    def _parse_line(self, line):
        if line and not line.startswith("//"):
            if line.startswith("\""):
                another_quote_index = line.find("\"", 1)
                if another_quote_index < 0:
                    raise Exception("that's only one quote in a string line")
                self.cur_tokens.append(line[0: another_quote_index + 1])
                self._parse_line(line[another_quote_index + 1:])


class Token:
    T_KEYWORD = 0,
    T_SYMBOL = 1,
    T_IDENTIFIER = 2,
    T_INT_CONST = 3,
    T_STRING_CONST = 4,

    KEYWORDS = ["class", "method", "int", "function", "boolean", "constructor", "char", "void", "var", "static", "field", "let", "do", "if", "else", "while", "return", "true", "false", "null", "this"]
    SYMBOLS = ['{', '}', '(', ')', '[', ']', '.', ',', ';', '+', '-', '*', '/', '&', '|', '<', '>', '=', '~']

    TOKEN_STR = {
        T_KEYWORD: "T_KEYWORD",
        T_SYMBOL: "T_KEYWORD",
        T_IDENTIFIER: "T_KEYWORD",
        T_INT_CONST: "T_KEYWORD",
        T_STRING_CONST: "T_KEYWORD",
    }

    _simple_type_str = {
        T_KEYWORD: "keyword",
        T_SYMBOL: "symbol",
        T_IDENTIFIER: "identifier",
        T_INT_CONST: "integerConstant",
        T_STRING_CONST: "stringConstant",
    }

    def __init__(self, token_src, token_type=None):
        self.src = token_src
        if token_type:
            self._type = token_type
        else:
            self._parse()

    def _parse(self):
        if self.src in Token.KEYWORDS:
            self._type = Token.T_KEYWORD
        else:
            self._type = Token.T_IDENTIFIER

    def token_type(self):
        return self._type

    def keyword(self):
        if self._type != Token.T_KEYWORD:
            raise Exception(f"token {self.src} is not a keyword")
        return self.src

    def symbol(self):
        if self._type != Token.T_SYMBOL:
            raise Exception(f"token {self.src} is not a symbol")
        return self.src

    def identifier(self):
        if self._type != Token.T_IDENTIFIER:
            raise Exception(f"token {self.src} is not a identifier")
        return self.src

    def int_val(self):
        if self._type != Token.T_INT_CONST:
            raise Exception(f"token {self.src} is not a int_val")
        return int(self.src)

    def string_val(self):
        if self._type != Token.T_STRING_CONST:
            raise Exception(f"token {self.src} is not a string_val")
        return int(self.src)

    def escape_str(self):
        if self.src == "<":
            return "&lt;"
        elif self.src == ">":
            return "&gt;"
        elif self.src == "&":
            return "&amp;"
        else:
            return self.src

    def __str__(self) -> str:
        return f"{Token.TOKEN_STR[self._type]}->{self.src}"

    def xml(self):
        tag = Token._simple_type_str[self.token_type()]
        return f"<{tag}> {self.escape_str()} </{tag}>"
