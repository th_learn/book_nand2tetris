import argparse
import os
import sys

from jackc.xml_writer.xml_compilation_engine import XmlCompilationEngine
from jackc.tokenizer import JackTokenizer


def writ_token(src_path, out_folder):
    """write token xml"""
    output_file_name = os.path.splitext(os.path.basename(src_path))[0] + ".xml"
    tokenizer = JackTokenizer(src_path)
    output_path = os.path.join(out_folder, output_file_name)
    if not os.path.exists(out_folder):
        os.makedirs(out_folder)
    out_file = open(output_path, "w")
    out_file.write("<tokens>\n")
    while tokenizer.has_more_tokens():
        token = tokenizer.advance()
        out_file.write(f"{token.xml()}\n")
    out_file.write("</tokens>")


def parse_single_file(src_path, out_folder):
    output_file_name = os.path.splitext(os.path.basename(src_path))[0] + ".xml"
    output_path = os.path.join(out_folder, output_file_name)
    XmlCompilationEngine(src_path, output_path).compile_class()


def main():
    parser = argparse.ArgumentParser(description="compiler jack files(single file or all file in folder)")
    parser.add_argument("src_path", type=str)
    parser.add_argument("-d", dest="out_folder")
    args = parser.parse_args()
    src_path = args.src_path
    if not src_path:
        print(f"Usage: {sys.argv[0]} [-d dst_folder]")
    out_folder = args.out_folder
    if not os.path.exists(src_path):
        print(f"src_path {src_path} not exists")
        return

    if os.path.isdir(src_path):
        out_folder = out_folder or src_path
        for f in os.listdir(src_path):
            if f.endswith(".jack"):
                parse_single_file(os.path.join(src_path, f), out_folder)

    else:
        out_folder = out_folder or os.path.dirname(src_path)
        parse_single_file(src_path, out_folder)


if __name__ == '__main__':
    main()
