from setuptools import setup, find_packages

setup(
    name='jackc',
    version='1.0.0',
    description='jack compiler',
    author="th",
    packages=find_packages(), install_requires=[],
    entry_points={
        'console_scripts': ['jackc=jackc.analyzer:main']
    }
)