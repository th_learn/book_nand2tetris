def dest(dest_str):
    rst = ["0", "0", '0']
    if "A" in dest_str:
        rst[0] = '1'
    if 'D' in dest_str:
        rst[1] = '1'
    if 'M' in dest_str:
        rst[2] = '1'
    return "".join(rst)


_comp_map = {
    "0":   "0101010",
    "1":   "0111111",
    "-1":  "0111010",
    "D":   "0001100",
    "A":   "0110000",
    "!D":  "0001101",
    "!A":  "0110001",
    "-D":  "0001111",
    "-A":  "0110011",
    "D+1": "0011111",
    "A+1": "0110111",
    "D-1": "0001110",
    "A-1": "0110010",
    "D+A": "0000010",
    "D-A": "0010011",
    "A-D": "0000111",
    "D&A": "0000000",
    "D|A": "0010101",
    "M":   "1110000",
    "!M":  "1110001",
    "-M":  "1110011",
    "M+1": "1110111",
    "M-1": "1110010",
    "D+M": "1000010",
    "D-M": "1010011",
    "M-D": "1000111",
    "D&M": "1000000",
    "D|M": "1010101"
}


def comp(comp_str):
    if comp_str in _comp_map:
        return _comp_map[comp_str]
    else:
        raise Exception("Invalidate comp: " + comp_str)


def jump(jump_str):
    if not jump_str:
        return "000"
    elif 'E' == jump_str[1]:
        return "010"
    elif 'N' == jump_str[1]:
        return "101"
    elif 'M' == jump_str[1]:
        return "111"

    rst = ["0", "0", "0"]
    if 'G' == jump_str[1]:
        rst[2] = '1'
    elif 'L' == jump_str[1]:
        rst[0] = '1'

    if 'E' == jump_str[2]:
        rst[1] = '1'
    return "".join(rst)
