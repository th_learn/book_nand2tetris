from jack_vm.parser import Command
from typing import Callable
import os

SEG_TEMP = "temp"
SEG_LOCAL = "local"
SEG_CONSTANT = "constant"
SEG_STATIC = "static"
SEG_SP = "sp"
SEG_ARGUMENT = "argument"
SEG_THIS = "this"
SEG_THAT = "that"

SEGMENT_POS = {
    SEG_SP: "SP",       # R0
    SEG_LOCAL: "LCL",   # R1
    SEG_ARGUMENT: "ARG",  # R2
    SEG_THIS: "R3",
    SEG_THAT: "R4",
    SEG_TEMP: "R5",
    "pointer": "R3"
}

VAR_TEMP_RETURN = "v_temp_return"
VAR_TEMP_POP_DEST = "v_temp_pop_dest"

# the segment which is not a pointer, the pointer segment is which value is a address
DIRECT_SEGMENT = ["temp", "pointer"]


def _segment_is_pointer(segment: str):
    return segment not in DIRECT_SEGMENT


def raise_exception_when_operating_static(reg_name):
    if reg_name == SEG_STATIC:
        raise Exception(f"can't call _set_r_pointer_value_to_d with segment {SEG_STATIC}")


class CodeWrite:
    def __init__(self):
        self.file = None
        self.true_index = 0
        self.base_file_name = ""
        self._write_map = {
            Command.C_ARITHMETIC: self._write_arithmetic,
            Command.C_PUSH: self._write_push_pop,
            Command.C_POP: self._write_push_pop,
            Command.C_LABEL: self._write_label,
            Command.C_IF: self._write_if,
            Command.C_GOTO: self._write_goto,
            Command.C_RETURN: self._write_return,
            Command.C_FUNCTION: self._write_function,
            Command.C_CALL: self._write_call,
        }
        self._call_index = 0
        self._cur_fun_name = ""

    def set_asm_file_path(self, asm_file_path: str):
        self.file = open(asm_file_path, "w")
        self.base_file_name = os.path.splitext(os.path.basename(asm_file_path))[0]

    def set_cur_src_path(self, src_path):
        self.base_file_name = os.path.splitext(os.path.basename(src_path))[0]

    def write(self, command):
        if command.c_type() in self._write_map:
            self._write_map[command.c_type()](command)
        else:
            raise Exception(f"Can't write command type: {command.c_type()}")

    def write_comment(self, comment):
        self._write(f"// {comment}")

    def write_init(self):
        self._write("// bootstrap")
        self._write("@256")
        self._write("D=A")
        self._write("@SP")
        self._write("M=D")
        self._write("\n")
        self.write(Command("call Sys.init 0"))
        self._write("\n")

    def _write_return(self, command: Command):
        self._set_reg_pointer_value_to_d(SEG_LOCAL, -5)
        self._write(f"@{VAR_TEMP_RETURN}")
        self._write("M=D")
        # self._set_d_to_reg_value(SEG_TEMP)
        # self._set_d_to_reg_pointer_value(SEG_ARGUMENT)
        self.write(Command("pop argument 0"))

        self._write("@ARG")
        self._write("D=M+1")
        self._set_d_to_reg_value(SEG_SP)

        self._set_reg_pointer_value_to_d(SEG_LOCAL, -1)
        self._set_d_to_reg_value("that")

        self._set_reg_pointer_value_to_d(SEG_LOCAL, -2)
        self._set_d_to_reg_value("this")

        self._set_reg_pointer_value_to_d(SEG_LOCAL, -3)
        self._set_d_to_reg_value("argument")

        self._set_reg_pointer_value_to_d(SEG_LOCAL, -4)
        self._set_d_to_reg_value(SEG_LOCAL)

        self._write(f"@{VAR_TEMP_RETURN}")
        self._write("D=M")
        # self._set_reg_value_to_d(SEG_TEMP)
        self._write("A=D")
        self._write("0;JMP")

    def _write_call(self, command: Command):
        fun_name = command.arg1()
        num_param = command.arg2()
        return_address = f"{fun_name}.return.{self._call_index}"
        self._call_index += 1
        self._write(f"@{return_address}")
        self._write("D=A")
        self._h_push_d()
        self._push_seg(SEG_LOCAL)
        self._push_seg(SEG_ARGUMENT)
        self._push_seg(SEG_THIS)
        self._push_seg(SEG_THAT)
        self._set_reg_value_to_d(SEG_SP)
        self._write(f"@{num_param}")
        self._write("D=D-A")
        self._write("@5")
        self._write("D=D-A")
        self._set_d_to_reg_value(SEG_ARGUMENT)
        self._set_reg_value_to_d(SEG_SP)
        self._set_d_to_reg_value(SEG_LOCAL)
        self._write(f"@{fun_name}")
        self._write(f"0;JMP")
        self._write(f"({return_address})")

    def _write_function(self, command: Command):
        fun_name = command.arg1()
        self._cur_fun_name = fun_name
        fun_num_local = command.arg2()
        self._write(f"({fun_name})")
        for i in range(0, int(fun_num_local)):
            self.write(Command(f"push {SEG_CONSTANT} 0"))

    def _write_goto(self, command: Command):
        self._write(f"@{self._fun_label(command.arg1())}")
        self._write("D;JMP")

    def _write_if(self, command: Command):
        self.write(Command(f"pop {SEG_TEMP} 0"))
        self._write_temp_to_d()
        self._write(f"@{self._fun_label(command.arg1())}")
        self._write("D;JNE")

    def _write_label(self, command: Command):
        self._write(f"({self._fun_label(command.arg1())})")

    def _write_arithmetic(self, command: Command):
        if command.arg1() == "add":
            self._quick_arithmetic_op_2(lambda: self._write("D=D+M"))
        elif command.arg1() == "sub":
            self._quick_arithmetic_op_2(lambda: self._write("D=D-M"))
        elif command.arg1() == "and":
            self._quick_arithmetic_op_2(lambda: self._write("D=D&M"))
        elif command.arg1() == "or":
            self._quick_arithmetic_op_2(lambda: self._write("D=D|M"))

        elif command.arg1() == "eq":
            self._quick_arithmetic_op_2(self._write_eq)
        elif command.arg1() == "lt":
            self._quick_arithmetic_op_2(self._write_lt)
        elif command.arg1() == "gt":
            self._quick_arithmetic_op_2(self._write_gt)

        elif command.arg1() == "neg":
            self._quick_arithmetic_op_1(lambda: self._write("M=-M"))
        elif command.arg1() == "not":
            self._quick_arithmetic_op_1(lambda: self._write("M=!M"))
        else:
            raise Exception(f"unknown arithmetic {command.arg1()}")

    def _write_push_pop(self, command: Command):
        segment = command.arg1()
        value = command.arg2()
        if command.c_type() == Command.C_PUSH:
            if segment == SEG_CONSTANT:
                self._write(f"@{value}")
                self._write("D=A")
            elif segment in SEGMENT_POS:
                self._write(f"@{SEGMENT_POS[segment]}")
                if _segment_is_pointer(segment):
                    self._write("D=M")
                else:
                    self._write("D=A")
                self._write(f"@{value}")
                self._write("D=D+A")
                self._write("A=D")
                self._write("D=M")
            elif segment == SEG_STATIC:
                self._write(f"@{self.base_file_name}.{command.arg2()}")
                self._write(f"D=M")
            else:
                raise Exception(f"unknown segment: {segment}")
            # for now, D = the value
            self._h_push_d()
        elif command.c_type() == Command.C_POP:
            index = value
            if segment == "constant":
                raise Exception(f"Can't pop constant in command: {command.src}")
            elif segment in SEGMENT_POS:
                self._write(f"@{SEGMENT_POS[segment]}")
                if _segment_is_pointer(segment):
                    self._write("A=M")
                self._write("D=A")
                self._write(f"@{index}")
                self._write("D=D+A")
                self._write(f"@{VAR_TEMP_POP_DEST}")
                self._write(f"M=D")
            elif segment == SEG_STATIC:
                self._write(f"@{self.base_file_name}.{command.arg2()}")
                self._write(f"D=A")
                self._write(f"@{VAR_TEMP_POP_DEST}")
                self._write(f"M=D")
            else:
                raise Exception(f"unknown segment {segment}")

            # for now, M[tmp] = pop dst
            self._set_a_to_sp()
            self._write("D=M")

            self._write(f"@{VAR_TEMP_POP_DEST}")
            self._write("A=M")
            self._write("M=D")
            self._write("@SP")
            self._write("M=M-1")

    # helper method
    def _fun_label(self, label):
        if self._cur_fun_name:
            return f"{self._cur_fun_name}${label}"
        else:
            return f"{label}"

    def _set_reg_pointer_value_to_d(self, seg_name, offset=0):
        """D = *(r_name +/- offset)"""
        raise_exception_when_operating_static(seg_name)
        is_reverse_direction = offset < 0
        self._write(f"@{SEGMENT_POS[seg_name]}")
        self._write("D=M")
        self._write(f"@{abs(offset)}")
        if is_reverse_direction:
            self._write("D=D-A")
        else:
            self._write("D=D+A")
        self._write("A=D")
        self._write("D=M")

    def _set_reg_value_to_d(self, seg_name):
        """D = *(r_name - offset)"""
        raise_exception_when_operating_static(seg_name)
        self._write(f"@{SEGMENT_POS[seg_name]}")
        self._write("D=M")

    def _set_d_to_reg_value(self, reg_name):
        """reg_name = d"""
        raise_exception_when_operating_static(reg_name)
        self._write(f"@{SEGMENT_POS[reg_name]}")
        self._write("M=D")

    def _set_d_to_reg_pointer_value(self, reg_name):
        """reg_name = d"""
        raise_exception_when_operating_static(reg_name)
        self._write(f"@{SEGMENT_POS[reg_name]}")
        self._write("A=M")
        self._write("M=D")

    def _push_seg(self, name):
        self._write(f"@{SEGMENT_POS[name]}")
        self._write("D=M")
        self._h_push_d()

    def _h_push_d(self):
        self._write("@SP")
        self._write("A=M")
        self._write("M=D")
        self._push_adjust_sp()

    def _write_temp_to_d(self):
        self._set_reg_value_to_d(SEG_TEMP)

    def _set_a_to_sp(self):
        self._write("@SP")
        self._write("A=M-1")

    def _quick_arithmetic_op_2(self, write_op: Callable[[], None]):
        """
        D = x
        M = y
        write_op (result d is the result)
        """
        self._set_a_to_sp()
        self._write("A=A-1")
        self._write("D=M")
        self._write("A=A+1")
        write_op()
        self._set_a_to_sp()
        self._write("A=A-1")
        self._write("M=D")
        self._write("@SP")
        self._write("M=M-1")

    def _quick_arithmetic_op_1(self, write_op: Callable[[], None]):
        """
        M = x
        write_op (result D = result)
        """
        self._set_a_to_sp()
        write_op()

    def _write_boolean(self, d_true_condition):
        """if d_true_condition D=0xFFFF
            else D = 0"""
        self._write(f"@D_{self.true_index}_TRUE")
        self._write(f"D;{d_true_condition}")
        self._write("D=0")
        self._write(f"@D_{self.true_index}_TRUE_END")
        self._write("0;JMP")
        self._write(f"(D_{self.true_index}_TRUE)")
        self._write("@32767")
        self._write("D=A")
        self._write("D=D+A")
        self._write("D=D+1")
        self._write(f"(D_{self.true_index}_TRUE_END)")
        self.true_index += 1

    def _write_eq(self):
        self._write("D=D-M")
        self._write_boolean("JEQ")

    def _write_lt(self):
        self._write("D=D-M")
        self._write_boolean("JLT")

    def _write_gt(self):
        self._write("D=D-M")
        self._write_boolean("JGT")

    def _write(self, command_str):
        print(command_str, file=self.file)

    def _push_adjust_sp(self):
        self._write("@SP")
        self._write("M=M+1")

    def close(self):
        self.file.close()
