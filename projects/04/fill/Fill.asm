// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

// The algorithm
// while(ture) {
//   if (pressed) {
//     screen[pos++] = black
//   } else {
//     screen[--pos] = white 
//   }
// }
@SCREEN
D=A
@clean_index
M=D
@set_index
M=D

(LOOP)
@KBD
D=M
@CLEAN
D;JEQ

@set_index
D=M
@24575
D=D-A
@BEGIN_SET
D;JLE

@16384
D=A
@set_index
M=D

(BEGIN_SET)
@set_index
A=M
D=0
D=D-1
M=D
@set_index
M=M+1

@LOOP
0;JMP


// clean screen begin
(CLEAN)
@clean_index
D=M
@24575
D=D-A
@BEGIN_CLEAN
D;JLE

@16384
D=A
@clean_index
M=D

(BEGIN_CLEAN)
@clean_index
A=M
M=0
@clean_index
M=M+1
@LOOP
0;JMP