import os
from enum import Enum, auto, unique
from typing import List, Union, Optional, Dict

from jackc.symbol_table import SymbolTable, SymbolKind, Symbol
from jackc.tokenizer import JackTokenizer, Token
from jackc.vmwriter import Segment, Arithmetic
from jackc.vmwriter import VMWriter


@unique
class SubroutineType(Enum):
    CONSTRUCTOR = 'constructor',
    FUNCTION = 'function',
    METHOD = 'method'


def _token_is_field(token):
    return token.src == "static" or token.src == "field"


def _token_is_subroutine(token: Token):
    return token.src == "constructor" or token.src == "function" or token.src == "method"


def _get_subroutine_type(subroutine_type: str) -> SubroutineType:
    return SubroutineType[subroutine_type.upper()]


def _token_is_statement(token: Token):
    return token.src == "let" or token.src == "if" or token.src == "while" or token.src == "do" \
           or token.src == "return"


ops = ['+', '-', '*', '/', '&', '|', '<', '>', '=']
op_arithmetic = {
    '+': Arithmetic.ADD,
    '-': Arithmetic.SUB,
    '&': Arithmetic.AND,
    '|': Arithmetic.OR,
    '>': Arithmetic.GT,
    '<': Arithmetic.LT,
    '=': Arithmetic.EQ,
}

unary_op_arithmetic = {
    '-': Arithmetic.NEG,
    '~': Arithmetic.NOT,
}

unary_op = ['-', '~']
keyword_constant = ['true', 'false', 'null', 'this']


class ClassInfo:
    def __init__(self):
        self.num_field = 0


_class_map: Dict[str, ClassInfo] = {}


class IfLabel:
    def __init__(self, func_name, index):
        self.func_name = func_name
        self.index = index

    def label_else(self) -> str:
        return f"else_{self.func_name}_{self.index}"

    def label_if_end(self) -> str:
        return f"fi_{self.func_name}_{self.index}"


class WhileLabel:
    def __init__(self, func_name, index):
        self.func_name = func_name
        self.index = index

    def label_begin(self) -> str:
        return f"while_begin_{self.func_name}_{self.index}"

    def label_end(self) -> str:
        return f"while_end_{self.func_name}_{self.index}"


def _is_subroutine_static(caller_name: str) -> bool:
    """function """
    return caller_name[0].isupper()


class CompilationEngine:
    """
    compile src_path which is jack lang to vm code and write to output_file
    @:param src_path the src file path
    @:param output_path where to write vm file
    """
    def __init__(self, src_path, output_path):
        self.tokenizer = JackTokenizer(src_path)

        out_folder = os.path.dirname(output_path)
        if not os.path.exists(out_folder):
            os.makedirs(out_folder)
        self.ahead_token = None

        self._class_name = ""
        self._func_name = ""
        self._writer = VMWriter(open(output_path, "w"))
        self._table: SymbolTable = SymbolTable()

        self._if_label_index = 0
        self._while_label_index = 0

    def compile_class(self):
        self._next_token("class")
        self._class_name = self._next_token().src
        self._next_token()
        self.compile_class_var_dec()
        self.compile_subroutine()
        self._next_token("}")

    def compile_class_var_dec(self):
        """
        :return: num of the field
        """
        token = self._read_ahead_next()
        if token and _token_is_field(token):
            symbol_kind = self._next_token_as_symbol_kink()
            symbol_type = self._next_token().src
            symbol_name = self._next_token().src
            self._table.define(symbol_name, symbol_type, symbol_kind)
            token = self._read_ahead_next()
            while token.src == ",":
                self._next_token(",")
                symbol_name = self._next_token().src
                self._table.define(symbol_name, symbol_type, symbol_kind)
                token = self._read_ahead_next()
            self._next_token(";")
            self.compile_class_var_dec()

    def compile_subroutine(self):
        token = self._read_ahead_next()
        while token and _token_is_subroutine(token):
            # function
            subroutine_type = _get_subroutine_type(self._next_token().src)
            self._next_token()
            func_name = self._class_name + "." + self._next_token().src
            self._func_name = func_name
            self._table.start_subroutine()
            self._next_token('(')
            if subroutine_type == SubroutineType.METHOD:
                self._table.define("this", self._class_name, SymbolKind.ARG)
            self.compile_parameter_list()
            self._next_token(')')
            # subroutineBody
            self._next_token('{')
            var_count = 0
            while self._read_ahead_next().src == "var":
                item_count = self.compile_var_dec()
                var_count += item_count
            self._writer.write_function(func_name, var_count)

            if subroutine_type == SubroutineType.METHOD:
                self._writer.write_push(Segment.ARG, 0)
                self._writer.write_pop(Segment.POINTER, 0)
            elif subroutine_type == SubroutineType.CONSTRUCTOR:
                field_count = self._table.var_count(SymbolKind.FIELD)
                self._writer.write_push(Segment.CONST, field_count)
                self._writer.write_call("Memory.alloc", 1)
                self._writer.write_pop(Segment.POINTER, 0)
            elif subroutine_type != SubroutineType.FUNCTION:
                raise Exception(f"Unknown subroutine type: {subroutine_type}")

            self.compile_statements()
            self._next_token('}')
            token = self._read_ahead_next()

    def compile_parameter_list(self) -> int:
        token = self._read_ahead_next()
        count = 0
        if token.src != ")":
            count += 1
            arg_type = self._next_token().src
            arg_name = self._next_token().src
            self._table.define(arg_name, arg_type, SymbolKind.ARG)
            token = self._read_ahead_next()
            while token.src == ",":
                count += 1
                self._next_token(",")
                arg_type = self._next_token().src
                arg_name = self._next_token().src
                self._table.define(arg_name, arg_type, SymbolKind.ARG)
                token = self._read_ahead_next()
        return count

    def compile_var_dec(self) -> int:
        """
        :return: local var count
        """
        self._next_token("var")
        var_type = self._next_token().src
        var_name = self._next_token().src
        self._table.define(var_name, var_type, SymbolKind.VAR)
        count = 1
        while self._read_ahead_next().src == ",":
            count += 1
            self._next_token(",")
            var_name = self._next_token().src
            self._table.define(var_name, var_type, SymbolKind.VAR)
        self._next_token(";")
        return count

    def compile_statements(self):
        token = self._read_ahead_next()
        while token and _token_is_statement(token):
            if token.src == "let":
                self.compile_let()
            elif token.src == "if":
                self.compile_if()
            elif token.src == "while":
                self.compile_while()
            elif token.src == "do":
                self.compile_do()
            elif token.src == "return":
                self.compile_return()
            token = self._read_ahead_next()

    def compile_do(self):
        self._next_token("do")
        self._compile_subroutine_call()
        self._writer.write_pop(Segment.TEMP, 0)
        self._next_token(";")

    def compile_let(self):
        self._next_token("let")
        var_symbol = self._table.find_symbol(self._next_token().src)
        has_index = False
        if self._read_ahead_next().src == "[":
            has_index = True
            self._push_symbol_as_base_p(var_symbol)
            self._next_token("[")
            self.compile_expression()
            self._next_token("]")
            self._writer.write_arithmetic(Arithmetic.ADD)

        self._next_token("=")
        self.compile_expression()

        if not has_index:
            self._pop_symbol(var_symbol)
        else:
            # stack: a[i], value
            self._writer.write_pop(Segment.TEMP, 0)
            self._writer.write_pop(Segment.POINTER, 1)
            self._writer.write_push(Segment.TEMP, 0)
            self._writer.write_pop(Segment.THAT, 0)
        self._next_token(";")

    def compile_while(self):
        while_label = self._new_while_label()
        self._new_while_label()
        self._next_token("while")
        self._next_token("(")
        self._writer.write_label(while_label.label_begin())
        self.compile_expression()
        self._writer.write_arithmetic(Arithmetic.NOT)
        self._writer.write_if(while_label.label_end())
        self._next_token(")")
        self._next_token("{")
        self.compile_statements()
        self._next_token("}")
        self._writer.write_goto(while_label.label_begin())
        self._writer.write_label(while_label.label_end())

    def compile_return(self):
        self._next_token("return")
        token = self._read_ahead_next()
        if token.src != ";":
            self.compile_expression()
        self._writer.write_return()
        self._next_token(";")

    def compile_if(self):
        if_label = IfLabel(self._func_name, self._if_label_index)
        self._new_if_label()
        self._next_token("if")
        self._next_token("(")
        self.compile_expression()
        self._next_token(")")
        self._writer.write_arithmetic(Arithmetic.NOT)
        self._writer.write_if(if_label.label_else())
        self._next_token("{")
        self.compile_statements()
        self._next_token("}")
        self._writer.write_goto(if_label.label_if_end())
        self._writer.write_label(if_label.label_else())
        if self._read_ahead_next().src == "else":
            self._next_token("else")
            self._next_token("{")
            self.compile_statements()
            self._next_token("}")
        self._writer.write_label(if_label.label_if_end())

    def compile_expression(self):
        self.compile_term()
        token = self._read_ahead_next()
        while token and token.src in ops:
            op = token.src
            self._next_token(ops)
            self.compile_term()
            self._write_op(op)
            token = self._read_ahead_next()

    def compile_term(self):
        """
        integerConstant|stringConstant|keywordConstant
        |varName|varName'['expression']'|subroutineCall|
        '('expression')'|unaryOp term
        """
        token = self._read_ahead_next()
        t_type = token.token_type()
        # integerConstant, stringConstant, keywordConstant
        if t_type == Token.T_INT_CONST or t_type == Token.T_STRING_CONST or token.src in keyword_constant:
            token = self._next()
            self._write_term_constant(token)
        # unaryOp term
        elif token.src in unary_op:
            op = self._next_token(unary_op).src
            self.compile_term()
            self._write_unary_op(op)
        # (expression)
        elif token.src == "(":
            self._next_token("(")
            self.compile_expression()
            self._next_token(")")
        else:
            # varName|varName'['expression']'|subroutineCall|
            token = self._next()
            next_token = self._read_ahead_next()

            # varName[expression]
            if next_token.src == "[":
                symbol = self._table.find_symbol(token.src)
                self._push_symbol_as_base_p(symbol)
                self._next_token('[')
                self.compile_expression()
                self._next_token(']')
                self._writer.write_arithmetic(Arithmetic.ADD)
                self._writer.write_pop(Segment.POINTER, 1)
                self._writer.write_push(Segment.THAT, 0)
            # subroutineCall
            elif next_token.src == "(" or next_token.src == '.':
                self._compile_subroutine_call(token)
            # varName
            else:
                self._write_term_var(token)

    def _compile_subroutine_call(self, pre_token: Token = None):
        func_name = (pre_token or self._next()).src
        token = self._read_ahead_next()
        if token.src == "(":
            self._writer.write_push(Segment.POINTER, 0)
            self._next_token("(")
            num_param = self.compile_expression_list()
            self._next_token(")")
            self._writer.write_call(self._class_name + "." + func_name, num_param + 1)
        else:
            var_name = func_name
            if not _is_subroutine_static(var_name):
                self._push_symbol(self._table.find_symbol(var_name))
            self._next_token(".")
            func_name = self._next_token().src
            self._next_token("(")
            num_param = self.compile_expression_list()
            self._next_token(")")
            if not _is_subroutine_static(var_name):
                num_param += 1
                cls_name = self._table.find_symbol(var_name).symbol_type
            else:
                cls_name = var_name

            self._writer.write_call(f"{cls_name}.{func_name}", num_param)

    def compile_expression_list(self) -> int:
        """
        :return: the num of the list
        """
        count = 0
        token = self._read_ahead_next()
        if token.src != ")":
            self.compile_expression()
            count += 1
            token = self._read_ahead_next()
            while token.src == ",":
                count += 1
                self._next_token()
                self.compile_expression()
                token = self._read_ahead_next()
        return count

    def _next_token(self, expect: Union[str,  List[str]] = "") -> Token:
        token = self._next()
        if expect:
            if isinstance(expect, str) and token.src != expect:
                raise Exception(f"want {expect}, but got {token.src}, at L{self.tokenizer.line_num()}")
            elif isinstance(expect, list) and token.src not in expect:
                raise Exception(f"want {expect}, but got {token.src}, at L{self.tokenizer.line_num()}")
        return token

    def _read_ahead_next(self) -> Token:
        rst = self._next()
        self.putback(rst)
        return rst

    def _next(self) -> Optional[Token]:
        if self.ahead_token:
            rst = self.ahead_token
            self.ahead_token = None
            return rst
        else:
            if self.tokenizer.has_more_tokens():
                return self.tokenizer.advance()
            else:
                return None

    def putback(self, token):
        if self.ahead_token:
            raise Exception("Why put back twice?")
        else:
            self.ahead_token = token

    def _next_token_as_symbol_kink(self) -> SymbolKind:
        token = self._next_token()
        return CompilationEngine._get_symbol_type(token.src)

    def _push_symbol_as_base_p(self, symbol: Symbol):
        """
        base[xx]
        """
        if symbol.kind == SymbolKind.STATIC or symbol.kind == SymbolKind.ARG or \
                symbol.kind == SymbolKind.VAR:
            self._push_symbol(symbol)
        elif symbol.kind == SymbolKind.FIELD:
            self._writer.write_push(Segment.POINTER, 0)
            self._writer.write_push(Segment.STATIC, symbol.index)
            self._writer.write_arithmetic(Arithmetic.ADD)
        else:
            raise Exception(f"unknown symbol kind: {symbol.kind}")

    def _push_symbol(self, symbol: Symbol):
        self._push_or_pop_symbol(symbol, "push")

    def _pop_symbol(self, symbol: Symbol):
        self._push_or_pop_symbol(symbol, "pop")

    def _push_or_pop_symbol(self, symbol: Symbol, push_or_pop: str):
        """
        :param push_or_pop: "push" or "pop"
        """
        if push_or_pop == "push":
            write_func = self._writer.write_push
        else:
            write_func = self._writer.write_pop

        if symbol.kind == SymbolKind.FIELD:
            self._writer.write_push(Segment.POINTER, 0)
            self._writer.write_push(Segment.CONST, symbol.index)
            self._writer.write_arithmetic(Arithmetic.ADD)
            self._writer.write_pop(Segment.POINTER, 1)
            write_func(Segment.THAT, 0)
        else:
            if symbol.kind == SymbolKind.STATIC:
                segment = Segment.STATIC
            elif symbol.kind == SymbolKind.ARG:
                segment = Segment.ARG
            elif symbol.kind == SymbolKind.VAR:
                segment = Segment.LOCAL
            else:
                raise Exception(f"symbol {symbol.name} unknown symbol kind: {symbol.kind.name}")
            write_func(segment, symbol.index)

    @staticmethod
    def _get_symbol_type(symbol_src: str) -> SymbolKind:
        return SymbolKind[symbol_src.upper()]

    def _new_if_label(self):
        rst = IfLabel(self._func_name, self._if_label_index)
        self._if_label_index += 1
        return rst

    def _new_while_label(self):
        rst = WhileLabel(self._func_name, self._while_label_index)
        self._while_label_index += 1
        return rst

    def _write_op(self, op):
        if op == "/":
            self._writer.write_call("Math.divide", 2)
        elif op == "*":
            self._writer.write_call("Math.multiply", 2)
        elif op in op_arithmetic:
            self._writer.write_arithmetic(op_arithmetic[op])
        else:
            raise Exception(f"write_op can't handle op {op}")

    def _write_unary_op(self, op):
        if op in unary_op_arithmetic:
            self._writer.write_arithmetic(unary_op_arithmetic[op])
        else:
            raise Exception(f"write_unary_op can't handle op {op}")

    def _write_term_constant(self, token):
        # print(token.src)
        if token.token_type() == Token.T_STRING_CONST:
            the_str = token.src
            len_the_str = len(the_str)
            self._writer.write_push(Segment.CONST, len_the_str)
            self._writer.write_call("String.new", 1)
            for c in the_str:
                c_ascii = ord(c)
                self._writer.write_push(Segment.CONST, c_ascii)
                self._writer.write_call("String.appendChar", 2)
        elif token.token_type() == Token.T_INT_CONST:
            int_val = int(token.src)
            if int_val < 0:
                int_val = -int_val
                self._writer.write_push(Segment.CONST, int_val)
                self._writer.write_arithmetic(Arithmetic.NEG)
            else:
                self._writer.write_push(Segment.CONST, int_val)
        elif token.src in keyword_constant:
            if token.src == "true":
                self._writer.write_push(Segment.CONST, 1)
                self._writer.write_arithmetic(Arithmetic.NEG)
            elif token.src == "false" or token.src == "null":
                self._writer.write_push(Segment.CONST, 0)
            elif token.src == "this":
                self._writer.write_push(Segment.POINTER, 0)
            else:
                raise Exception(f"unknown keyword constant: {token.src}")

    def _write_term_var(self, token):
        """
        write term the varName branch
        """
        if token.token_type() == Token.T_IDENTIFIER:
            self._push_symbol(self._table.find_symbol(token.src))
        else:
            raise Exception(f"Can't handle term_var {token.src}, token_type: {token.token_type()}")

    def _moving_space(self, size):
        for i in range(0, size):
            self._writer.write_push(Segment.CONST, 0)

    def _recycle_space(self, size):
        for i in range(0, size):
            self._writer.write_pop(Segment.TEMP, 0)
