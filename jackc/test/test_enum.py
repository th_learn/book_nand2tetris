from enum import Enum, unique


@unique
class SubroutineType(Enum):
    CONSTRUCTOR = 'constructor',
    FUNCTION = 'function',
    METHOD = 'method'


print(SubroutineType.FUNCTION)
print(SubroutineType['FUNCTION'])
