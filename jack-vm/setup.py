from setuptools import setup, find_packages

setup(
    name='jack_vm',
    version='1.0.0',
    description='trans jack vm to assemble',
    author="th",
    packages=find_packages(), install_requires=[],
    entry_points={
        'console_scripts': ['jack_vm=jack_vm.main:main']
    }
)