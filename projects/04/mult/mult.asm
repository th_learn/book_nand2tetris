// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.
@sum
M=0
@i
M=0
(MULT)
@i
D=M
@0
D=D-M
@FINISH
D;JGE
@sum
D=M
@1
D=D+M
@sum
M=D
@i
M=M+1
@MULT
0;JMP
(FINISH)
@sum
D=M
@2
M=D
(END)
@END
0;JMP