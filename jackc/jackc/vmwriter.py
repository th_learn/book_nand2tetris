from enum import IntEnum, unique, auto


_segment_names = {
    "CONST": "constant",
    "ARG": "argument"
}


@unique
class Segment(IntEnum):
    CONST = 0,
    ARG = 1,
    LOCAL = 2,
    STATIC = 3,

    # 任何vm函数可以使用这两个段来操纵堆中指定的区域
    THIS = 4,
    THAT = 5,
    POINTER = 6,
    TEMP = 7

    def segment_name(self) -> str:
        if self.name in _segment_names:
            return _segment_names[self.name]
        return self.name.lower()


class Arithmetic(IntEnum):
    ADD = 0,
    SUB = 1,
    NEG = 2,
    EQ = 3,
    GT = 4,
    LT = 5,
    AND = 6,
    OR = 7,
    NOT = 8

    def vm_name(self):
        return self.name.lower()


class VMWriter:
    def __init__(self, output_file):
        self.file = output_file

    def write_push(self, segment: Segment, index: int):
        self._push_or_pop_segment(segment, index, "push")

    def write_pop(self, segment: Segment, index: int):
        self._push_or_pop_segment(segment, index, "pop")

    def write_arithmetic(self, arith: Arithmetic):
        self._write(f"{arith.vm_name()}")

    def write_label(self, label: str):
        self._write(f"label {label}")

    def write_goto(self, label: str):
        self._write(f"goto {label}")

    def write_if(self, label: str):
        self._write(f"if-goto {label}")

    def write_call(self, name: str, n_args: int):
        self._write(f"call {name} {n_args}")

    def write_function(self, name: str, n_args: int):
        self._write(f"function {name} {n_args}")

    def write_return(self):
        self._write(f"return")

    def close(self):
        self.file.close()

    def _write(self, content):
        print(content, file=self.file)

    def _push_or_pop_segment(self, segment: Segment, index: int, push_or_pop: str):
        """
        :param push_or_pop: "push" or "pop"
        """
        if segment == Segment.POINTER and not (index == 0 or index == 1):
            raise Exception(f"why {push_or_pop} pointer {index}")
        if segment == Segment.THIS and not (index == 0):
            raise Exception(f"you can't {push_or_pop} this != 0")
        if segment == Segment.THAT and not (index == 0):
            raise Exception(f"you can't {push_or_pop} that != 0")
        if segment == Segment.CONST and index < 0:
            raise Exception(f"you can't {push_or_pop} constant < 0")
        self._write(f"{push_or_pop} {segment.segment_name()} {index}")
