from enum import Enum, auto, unique

class ClassInfo:
    def __init__(self, name, num_field):
        self.name = name
        self.num_filed = num_field

@unique
class SubroutineType(Enum):
    pass


class SubroutineInfo:
    def __init__(self, name, subroutine_type):
        pass


class CrossClassTable:
    pass
