from hack_asm.parser import Parser
from hack_asm.parser import Command
from hack_asm.symbol_table import SymbolTable
import hack_asm.code as code
import sys
import os


def first_time(src_file_path):
    symbol_table = SymbolTable()
    parser = Parser(src_file_path)
    command = parser.advance()
    index = 0
    while command:
        if command.command_type() == Command.L_COMMAND:
            symbol_table.add_entry(command.symbol(), index)
        else:
            index += 1
        command = parser.advance()
    return symbol_table


def second_time(src_file_path, out, symbol_table: SymbolTable):
    parser = Parser(src_file_path)
    command = parser.advance()
    variable_index = 16
    while command:
        if command.type == Command.C_COMMAND:
            dest = code.dest(command.dest())
            comp = code.comp(command.comp())
            jump = code.jump(command.jump())
            print("111{}{}{}".format(comp, dest, jump), file=out)
        elif command.type == Command.A_COMMAND:
            symbol = command.symbol()
            if symbol[0].isdigit():
                value = int(symbol)
            else:
                if symbol_table.contains(symbol):
                    value = symbol_table.get_address(symbol)
                else:
                    value = variable_index
                    variable_index += 1
                    symbol_table.add_entry(symbol, value)

            print(f"0{value:015b}", file=out)
        command = parser.advance()


def main():
    if len(sys.argv) == 1:
        print("usage: {} source [dst]".format(sys.argv[0]))
        return

    src_file_path = sys.argv[1]
    if len(sys.argv) >= 3:
        dst_file_path = sys.argv[2]
    else:
        dst_file_path = os.path.splitext(src_file_path)[0] + ".hack"

    second_time(src_file_path, open(dst_file_path, "w"), first_time(src_file_path))


if __name__ == '__main__':
    main()
