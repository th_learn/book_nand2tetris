#encoding=utf-8
from setuptools import setup, find_packages

print("hello in setup.py")

setup(
    name='hack_asm',
    version='1.0.0',
    description='hack asm',
    author="th",
    scripts=["hack_asm/main.py"],
    packages=["hack_asm"], install_requires=[],
    entry_points={
        'console_scripts': ['hack-asm=hack_asm.main:main']
    }
)
